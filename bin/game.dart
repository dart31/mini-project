import 'dart:io';

class Map {
  var player;
  var monster;
  var guard;
  bool combat(String enemy) {
    if (enemy == "Guard") {
      print(
          "Guard ${guard.name}: Hey don't be stupid.\n\nThe guard hit you so hard and you gave up.\n(You receive ${guard.damage} damage)\n");
      guard.Attack(player);
      player.Dead();
      return true;
    } else if (enemy == "Monster") {
      while (true) {
        print(
            "\n------------------------------------------------------------------\n");
        print("Your HP: ${player.HP}");
        print("Monster HP: ${monster.HP}");
        print("\n1: Attack");
        print("2: Run");
        print(
            "\n------------------------------------------------------------------\n");
        String? ch_str = stdin.readLineSync();
        if (ch_str != null) {
          int choice = int.parse(ch_str);
          if (choice == 1) {
            print("You attacked the monster and gave ${player.damage} damage!");
            player.Attack(monster);
            if (monster.HP < 1) {
              monster.HP = 0;
              print("Monster HP: ${monster.HP}");
              return true;
            } else if (monster.HP > 0) {
              print(
                  "The monster attacked you and gave ${monster.damage} damage!");
              monster.Attack(player);
              if (player.HP < 1) {
                player.HP = 0;
                print("Player HP: ${player.HP}");
                player.Dead();
                return true;
              }
              print("Player HP: ${player.HP}");
            }
            print("Monster HP: ${monster.HP}");
          } else if (choice == 2) {
            break;
          }
        }
      }
    }
    return false;
  }

  MainPlayer(int HP, int Damage) {
    print("Please enter your name:");
    String name = stdin.readLineSync() as String;
    player = new Player(name, HP, "Knife", Damage, "Male");
  }

  MainGuard(int HP, int Damage) {
    guard = new Guard("Edward", HP, "Spear", Damage, "Male", "Blue");
  }

  MainMonster(int HP, int Damage) {
    monster =
        new Monster("Goblin", HP, "Axe", Damage, "Green", "Scrub", "Yellow", 2);
  }
}

class Game {
  var map;
  var silverRing = 0;
  Game() {
    map = new Map();
  }
  void showWelcome() {
    print(" ");
    print("┌─────── •✧✧• ───────┐ ");
    print("   ─Welcome to Game─    ");
    print("└─────── •✧✧• ───────┘");
    print(" ");
    map.MainGuard(100, 100);
    map.MainMonster(15, 5);
    map.MainPlayer(15, 4);
    print("Your HP: ${map.player.HP}");
    print("Your Weapon: ${map.player.weapon}");
    print(" ___ ");
    print("(._.)");
    print("<||> " + "   " + "[${map.player.name}]");
    print("_/|_");
    print(" ");
    print("Hello ${map.player.name}, let's start the game!");

    while (true) {
      print(
          "\n------------------------------------------------------------------\n");
      print("You are at the gate of the town.");
      print("A guard is standing in front of you.");
      print("");
      print("What do you want to do?");
      print("");
      print("1: Talk to the guard");
      print("2: Attack the guard");
      print("3: Leave");
      print(
          "\n------------------------------------------------------------------\n");
      String? ch_str = stdin.readLineSync();
      if (ch_str != null) {
        int choice = int.parse(ch_str);
        if (choice == 1) {
          map.guard.Talk(map.player.name);
        } else if (choice == 2) {
          if (map.combat("Guard")) {
            silverRing = -1;
            break;
          }
        } else if (choice == 3) {
          break;
        }
      }
    }
    while (true) {
      if (silverRing == 1) {
        showEndGame();
        break;
      } else if (silverRing == -1) {
        break;
      }
      print(
          "\n------------------------------------------------------------------\n");
      print(
          "You are at a crossroad. If you go south, you will go back to the town.\n\n");
      print("1: Go north");
      print("2: Go east");
      print("3: Go south");
      print("4: Go west");
      print(
          "\n------------------------------------------------------------------\n");
      String? ch_str = stdin.readLineSync();
      if (ch_str != null) {
        int choice = int.parse(ch_str);
        if (choice == 1) {
          while (true) {
            print(
                "\n------------------------------------------------------------------\n");
            print(
                "There is a river. You drink the water and rest at the riverside.");
            print("Your HP is recovered.");
            map.player.HP = map.player.HP + 1;
            print("Your HP: ${map.player.HP}");
            print("\n\n1: Go back to the crossroad");
            print(
                "\n------------------------------------------------------------------\n");
            String? ch_str = stdin.readLineSync();
            if (ch_str != null) {
              int choice = int.parse(ch_str);
              if (choice == 1) {
                break;
              }
            }
          }
        } else if (choice == 2) {
          while (true) {
            print(
                "\n------------------------------------------------------------------\n");
            print("You walked into a forest and found a Long Sword!");
            map.player.weapon = "Long Sword";
            map.player.damage = 8;
            print("Your Weapon: ${map.player.weapon}");
            print("\n\n1: Go back to the crossroad");
            print(
                "\n------------------------------------------------------------------\n");
            String? ch_str = stdin.readLineSync();
            if (ch_str != null) {
              int choice = int.parse(ch_str);
              if (choice == 1) {
                break;
              }
            }
          }
        } else if (choice == 3) {
          while (true) {
            print(
                "\n------------------------------------------------------------------\n");
            print("You are at the gate of the town.");
            print("A guard is standing in front of you.");
            print("");
            print("What do you want to do?");
            print("");
            print("1: Talk to the guard");
            print("2: Attack the guard");
            print("3: Leave");
            print(
                "\n------------------------------------------------------------------\n");
            String? ch_str = stdin.readLineSync();
            if (ch_str != null) {
              int choice = int.parse(ch_str);
              if (choice == 1) {
                if (silverRing == 1) {
                  showEndGame();
                } else {
                  map.guard.Talk(map.player.name);
                }
              } else if (choice == 2) {
                if (map.combat("Guard")) {
                  break;
                }
              } else if (choice == 3) {
                break;
              }
            }
          }
        } else if (choice == 4) {
          while (true) {
            print(
                "\n------------------------------------------------------------------\n");
            print("You encounter a ${map.monster.name}!\n");
            print("1: Fight");
            print("2: Run");
            print(
                "\n------------------------------------------------------------------\n");
            String? ch_str = stdin.readLineSync();
            if (ch_str != null) {
              int choice = int.parse(ch_str);
              if (choice == 1) {
                if (map.combat("Monster")) {
                  if (map.monster.HP == 0) {
                    showWin();
                    break;
                  } else if (map.player.HP == 0) {
                    silverRing = -1;
                    break;
                  }
                }
              } else if (choice == 2) {
                break;
              }
            }
          }
        }
      }
    }
  }

  void showWin() {
    while (true) {
      print(
          "\n------------------------------------------------------------------\n");
      print("You killed the monster!");
      map.monster.DropItem();
      print("1: Go east");
      print(
          "\n------------------------------------------------------------------\n");

      silverRing = 1;

      String? ch_str = stdin.readLineSync();
      if (ch_str != null) {
        int choice = int.parse(ch_str);
        if (choice == 1) {
          break;
        }
      }
    }
  }

  void showEndGame() {
    print(
        "\n------------------------------------------------------------------\n");
    map.guard.Trade(map.monster.name);
    print("\n\n           THE END                    ");
    print(
        "\n------------------------------------------------------------------\n");
  }
}

class Character {
  var name;
  var HP;
  var weapon;
  var damage;
  Character(String name, int HP, String weapon, int damage) {
    this.name = name;
    this.HP = HP;
    this.weapon = weapon;
    this.damage = damage;
  }
  void Attack(Character other) {
    other.HP = other.HP - damage;
  }
}

class Guard extends Character {
  var _Sex;
  var _colorHair;
  Guard(String name, int HP, String weapon, int damage, String Sex,
      String colorHair)
      : super(name, HP, weapon, damage) {
    _Sex = Sex;
    _colorHair = colorHair;
  }
  void Talk(String playerName) {
    print(
        "Guard: Hello there, stranger. So your name is ${playerName}? \nSorry but we cannot let stranger enter our town.");
  }

  void Trade(String monsterName) {
    print("Guard: Oh you killed that ${monsterName}!?? Great!");
    print("Guard: It seems you are a trustworthy guy. Welcome to our town!");
  }
}

class Player extends Character {
  var _Sex;
  Player(String name, int HP, String weapon, int damage, String Sex)
      : super(name, HP, weapon, damage) {
    _Sex = Sex;
  }
  void Dead() {
    print(
        "\n------------------------------------------------------------------\n");
    print("You are dead!!!");
    print("⚝──⭒─⭑─⭒──⚝");
    print("|GAME OVER|");
    print("⚝──⭒─⭑─⭒──⚝");
    // print("\n\nGAME OVER");
    print(
        "\n------------------------------------------------------------------\n");
  }
}

class Monster extends Character {
  var colorSkin;
  var shape;
  var colorEye;
  var NumberofArm;

  Monster(String name, int HP, String weapon, int damage, String colorSkin,
      String shape, String colorEye, int NumberofArm)
      : super(name, HP, weapon, damage) {
    this.colorSkin = colorSkin;
    this.shape = shape;
    this.colorEye = colorEye;
    this.NumberofArm = NumberofArm;
  }
  void DropItem() {
    print("The monster dropped a ring!");
    print("You obtaind a silver ring!\n\n");
  }

  void Dead() {
    print(
        "\n------------------------------------------------------------------\n");
    print("Monster are dead!!!");
    print(
        "\n------------------------------------------------------------------\n");
  }
}

void main() {
  Game game = new Game();
  game.showWelcome();
}
